package com.example.demo1;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Homework03August {
    WebDriver driver;
    HashMap<String, List<String>> expectedProductList = new HashMap<>();
    HashMap<String, List<String>> actualProductList = new HashMap<>();

    String emailLogin = "shopbase2@beeketing.net";
    String passwordLogin="*esAS!z(:YeZ-5q";

    String title= "Dây Kháng Lực Đa năng Tập Gym";
    String description = "Bộ sản phẩm dây ngũ sắc tập thể hình là sự kết hợp của 5 dây ngũ sắc kháng lực";

    float priceProduct = 10f;
    String input1=String.valueOf(priceProduct);
    float comparePrice= 20f;
    String input2 = String.valueOf(comparePrice);
    float costPrice= 2f;
    String input3=String.valueOf(costPrice);

    String sku="string";
    String barcode = "ASD123AA";
    int weight= 2;
    String input4=String.valueOf(weight);

    String emailPayment = "demo123@gmail.com";
    String lastName="Thang";
    String address1="Van Quan";
    String city = "Ha Noi";
    String phone ="0369852147";

    @Before
    public void setup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test
    public void createProduct() throws InterruptedException {
        //login shop
        driver.get("https://au-webhook-adc1.onshopbase.com/admin/");
        inputToElement("//input[@id='email']", emailLogin);
        inputToElement("//input[@id='password']",passwordLogin);
        clickOnButtonName("Sign in");
//        waitElementVisible("");



        //click create product
        waitElementVisible("//span[contains(text(),'Products')]");
        clickElement("//span[contains(text(),'Products')]");
//
//
//        //add title products
        clickElement("//span[contains(text(),'Add product')]");
        enterTextToTextbox("Short Sleeve T-Shirt",title);


//
//        //set Iframe description
//        driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='Rich Text Area. Press ALT-0 for help.']")));
//        driver.findElement(By.xpath("//body[@id='tinymce']//p")).sendKeys(description);
//        driver.switchTo().parentFrame();
//        driver.switchTo().defaultContent();
//
//        //set Pricing
//        driver.findElement(By.xpath("//input[@id='price']")).sendKeys(input1);
//        driver.findElement(By.xpath("//input[@id='compare_price']")).sendKeys(input2);
//        driver.findElement(By.xpath("//input[@id='cost_price']")).sendKeys(input3);
//
//        //set Inventory
//        driver.findElement(By.xpath("//input[@id='sku']")).sendKeys(sku);
//        driver.findElement(By.xpath("//input[@id='barcode']")).sendKeys(barcode);
//
//        //set Shipping
//        driver.findElement(By.xpath("(//body/div[@id='app']//input[@type='text'])[7]")).sendKeys(input4);
//
//        //set Organization
//        driver.findElement(By.xpath("(//body/div[@id='app']//input[@type='text'])[8]")).sendKeys("Sport");
//        driver.findElement(By.xpath("(//body/div[@id='app']//input[@type='text'])[9]")).sendKeys("Hoang-Thang");
//        driver.findElement(By.xpath("(//body/div[@id='app']//input[@type='text'])[10]")).sendKeys("Sport");
//        driver.findElement(By.xpath("(//body/div[@id='app']//input[@type='text'])[11]")).sendKeys("sports");
//        driver.findElement(By.xpath("//span[contains(text(),'Save product')]")).click();
    }

    @Test
    public void compareProduct(){
        driver.get("https://au-webhook-adc1.onshopbase.com/products/day-khang-luc-%3Fa-nang-tap-gym");
        //compare name
        String nameProduct = driver.findElement(By.xpath("//h1[@class='mb20 mt0 weight-400 product-name']")).getText();
        System.out.println("Name product: " + title.equals(nameProduct));
        System.out.println("---------------------------");

//        //compare price product
        String priceProduct1 = driver.findElement(By.xpath("//span[@class='weight-700']")).getText();
        String valuesPrice = "$"+priceProduct+"0";
        System.out.println("Price product: " +valuesPrice.equals(priceProduct1));
        System.out.println("---------------------------");

//        //compare price original
        String priceOriginal1 = driver.findElement(By.xpath("//span[@class='price-original']")).getText();
        String valuesPriceOriginal = "$"+comparePrice+"0";
        System.out.println("Price original product: " + priceOriginal1.equals(valuesPriceOriginal));
        System.out.println("---------------------------");

        //compare product detail
        String detailProduct = driver.findElement(By.xpath("//div[@itemprop='description']")).getText();
        System.out.println("Description product: "+description.equals(detailProduct));

//
//        System.out.println("--------------------------");
//        System.out.println("Name product: " + nameProduct +
//                "\nAnd priceProduct: " + priceProduct1 +
//                "\nPrice original : " + priceOriginal1 +
//                "\nProduct detail: : " + detailProduct);

    }

    @Test
    public void paymentMethod(){
        driver.get("https://au-webhook-adc1.onshopbase.com/checkouts/f632bb97ba5946b3a9b0a3f6c103874a?step=contact_information");
        driver.findElement(By.xpath("//input[@id='checkout_shipping_address_email']")).sendKeys(emailPayment);
        driver.findElement(By.xpath("//input[@id='checkout_shipping_address_last_name']")).sendKeys(lastName);
        driver.findElement(By.xpath("//input[@id='checkout_shipping_address_address_line1']")).sendKeys(address1);
        driver.findElement(By.xpath("//input[@id='checkout_shipping_address_city']")).sendKeys(city);
        WebElement phoneNumber = driver.findElement(By.xpath("//input[@id='checkout_shipping_address_phone']"));
        phoneNumber.clear();
        phoneNumber.sendKeys(phone);

    }
//
//    @After
//    public void finishTest() {
//        driver.quit();
//    }

    public void clickElement(String xpath) {
        waitElementVisible(xpath);
        getElement(xpath).click();
    }

    public WebElement getElement(String xpath) {
        return driver.findElement(By.xpath(xpath));

    }

    public void clickOnButtonName(String btnName) {
        clickElement("//button[normalize-space()='" + btnName + "']");
    }
    public String getElementText(String xpath) {
        waitElementVisible(xpath);
        return getElement(xpath).getText();
    }

    public void inputToElement(String xpath, String value) {
        waitElementVisible(xpath);
        WebElement e = getElement(xpath);
        e.clear();
        e.sendKeys(value);
    }

    public String getText(String xpath) {
        return driver.findElement(By.xpath(xpath)).getText();
    }

    public void waitElementVisible(String xpath) {
        WebDriverWait driverWait = new WebDriverWait(driver, 10);
        driverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xpath)));
    }

    public void clickOnCheckbox(String checkboxName) {
        clickElement("//span[normalize-space()='" + checkboxName + "']/preceding-sibling::input");
    }

    public void enterTextToTextbox(String name, String value) {
        inputToElement("//input[@placeholder='" + name + "']", value);
    }
    public void enterTextToIdTextbox(String name, String value) {
        inputToElement("//body[@id='" + name + "']", value);
    }


    public void switchToIframe(String xpath) {
        driver.switchTo().frame(xpath);
    }

    public void enterFieldInDescription(String iframeCard, String fieldName, String value) {
        driver.switchTo().parentFrame();
        driver.switchTo().frame(iframeCard);
        enterTextToIdTextbox(fieldName, value);
    }

}


