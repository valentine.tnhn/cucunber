package com.example.demo1;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class LoginShopBase {

    WebDriver driver;
    WebDriverWait driverWait;

    @Given("Setup chrome")
    public void setup_chrome() {
        WebDriverManager.chromedriver().setup();
        driver=new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Given("Login page")
    public void login_page() {
        driver.navigate().to("https://accounts.shopbase.com/sign-in?return_url=https%3A%2F%2Fau-webhook-adc1.onshopbase.com%2Fadmin%2F");
    }

    @When("User enter email and password")
    public void user_enter_email_and_password() {
        driver.findElement(By.id("email")).sendKeys("shopbase2@beeketing.net");
        driver.findElement(By.id("password")).sendKeys("*esAS!z(:YeZ-5q");
    }

    @When("User click button login")
    public void user_click_button_login() {
        driver.findElement(By.tagName("button")).click();
    }

    @When("^User enter email and password$")
    public void user_enter_email_and_password(List<List<String>> data) {
        String email = "", password = "", msg = "";
        int row = 0;
        for (int i = 0; i < data.size() - 1; i++) {
            email = data.get(i+1).get(0);
            password = data.get(i+1).get(1);
            msg = data.get(i+1).get(2);
            System.out.println("user name = " + email);
            System.out.println("password = " + password);
            System.out.println("msg = " + msg);
            System.out.println("-----------------------");
        }

        driver.close();
        driver.quit();
    }

}

