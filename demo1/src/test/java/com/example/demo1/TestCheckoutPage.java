package com.example.demo1;


import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class TestCheckoutPage {
    WebDriver driver;


    @BeforeClass
    public void testSetup() {
        WebDriverManager.chromedriver().setup();
        driver=new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @BeforeMethod
    public void openBrowser() {
        driver.get("https://au-webhook-adc1.onshopbase.com/collections/all");
        driver.findElement(By.xpath("//body/div[@id='app']/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[3]/div[1]/a[1]/div[1]/div[1]/img[1]")).click();
        driver.findElement(By.xpath("//button[@id='add-to-cart']")).click();
        driver.findElement(By.xpath("//body/div[@id='app']/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/div[2]/div[1]/button[1]")).click();
        System.out.println("By item " +driver.getCurrentUrl());
    }

    @Test(description="This method validates the checkout functionality")
    public void checkout() {
        driver.findElement(By.xpath("//input[@id='checkout_shipping_address_email']")).sendKeys("test.sele@gmail.com");
        driver.findElement(By.xpath("//input[@id='checkout_shipping_address_first_name']")).sendKeys("Hoang Trieu");
        driver.findElement(By.xpath("//input[@id='checkout_shipping_address_last_name']")).sendKeys("Thang");
        driver.findElement(By.xpath("//input[@id='checkout_shipping_address_address_line1']")).sendKeys("Van Quan");
        driver.findElement(By.xpath("//input[@id='checkout_shipping_address_address_line2']")).sendKeys("demo123");
        driver.findElement(By.xpath("//input[@id='checkout_shipping_address_city']")).sendKeys("Ha Noi");
        driver.findElement(By.xpath("//input[@id='checkout_shipping_address_phone']")).sendKeys("0369852147");
        driver.findElement(By.xpath("//input[@id='checkout_shipping_address_zip']")).sendKeys("2147");
        driver.findElement(By.xpath("//body/div[@id='app']/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[3]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]")).sendKeys("Testing");
        driver.findElement(By.xpath("//button[contains(text(),'Apply')]")).click();
        driver.findElement(By.xpath("//button[contains(text(),'Continue to shipping method')]")).click();
    }



    @AfterMethod
    public void postSignUp() {
        System.out.println(driver.getCurrentUrl());
    }

    @AfterClass
    public void afterClass(){
        driver.close();
        driver.quit();
    }

}

