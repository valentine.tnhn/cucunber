Feature: Test login Shop Base
  Scenario: Check login validate

    Given Setup chrome
    And Login page
    When User enter email and password
      | email                         | password        | messenger                      |
      | 12342@beeketing.net           | *esAS!z(:YeZ-5q | Email or password is not valid |
      | shopbas.e2@beeketing.net      | *esAS!z(:YeZ-5q | Email or password is not valid |
      | 123@gmail.com                 | *esAS!z(:YeZ-5q | Email or password is not valid |
      | !@#qwe123QEW@gmail.com        | *esAS!z(:YeZ-5q | Email or password is not valid |
      | lijoiejf-@gmail.com           | *esAS!z(:YeZ-5q | Email or password is not valid |
      | qwijwdw.mail.com@             | *esAS!z(:YeZ-5q | Email or password is not valid |
      | firstname.lastname@domain.com | *esAS!z(:YeZ-5q | Email or password is not valid |
      | 1234567890@domain.com         | *esAS!z(:YeZ-5q | Email or password is not valid |
      | shopbase2@beeketing.net       | *esAS!z(:YeZ-5q | Login success                  |
#    And User click button login

